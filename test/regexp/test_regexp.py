#!/usr/bin/env python3

import unittest

from felix_test_case import FelixTestCase


class TestRegexp(FelixTestCase):

    def test_regexp_name(self):
        self.assertRegex("felig", r"gbt|full|ltdb|fei4|itk-pixel|itk-strip|felig|full-mode-emulator")

    def test_regexp_ip(self):
        self.assertRegex("192.168.100.4", r"\d+\.\d+\.\d+\.\d+")


if __name__ == '__main__':
    unittest.main()
