#!/usr/bin/env python3

import unittest

from felix_test_case import FelixTestCase


class TestSupervisordError(FelixTestCase):

    def setUp(self):
        self.start('test-supervisord-error')

    def tearDown(self):
        self.stop('test-supervisord-error')

    def test_supervisord_error(self):
        pass


if __name__ == '__main__':
    unittest.main()
