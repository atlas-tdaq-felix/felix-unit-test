#!/usr/bin/env python3

import unittest

from felix_test_case import FelixTestCase


class TestPort(FelixTestCase):

    def test_port(self):
        port1 = FelixTestCase.get_port()
        port2 = FelixTestCase.get_port()
        port3 = FelixTestCase.get_port()
        self.assertNotEqual(port1, port2)
        self.assertNotEqual(port2, port3)
        self.assertNotEqual(port1, port3)

    def test_port_class(self):
        self.assertIsNotNone(FelixTestCase.port)
        self.assertIsNotNone(FelixTestCase.env.get('PORT'))
        self.assertIsNotNone(FelixTestCase.env.get('ALT_PORT'))
        self.assertEqual(FelixTestCase.port, FelixTestCase.env.get('PORT'))
        self.assertEqual(FelixTestCase.alt_port, FelixTestCase.env.get('ALT_PORT'))


if __name__ == '__main__':
    unittest.main()
