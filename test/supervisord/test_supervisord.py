#!/usr/bin/env python3

import os
import shutil
import time
import unittest

from felix_test_case import FelixTestCase


class TestSupervisord(FelixTestCase):

    def setUp(self):
        self.start('test-supervisord')

    def tearDown(self):
        self.stop('test-supervisord')

    def test_supervisord(self):
        time.sleep(3)
        managed_dir = "/tmp/felix-unit-test_" + FelixTestCase.uuid + "_managed/"
        self.assertTrue(os.path.isdir(managed_dir))
        shutil.rmtree(managed_dir, ignore_errors=True)


if __name__ == '__main__':
    unittest.main()
