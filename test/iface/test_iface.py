#!/usr/bin/env python3

import unittest

from felix_test_case import FelixTestCase


class TestIface(FelixTestCase):

    def test_iface(self):
        iface = FelixTestCase.get_inet_iface(['lo', 'priv0', 'priv1', 'eth0', 'ctrl0'])
        self.assertEqual(iface.get('name'), 'lo')
        self.assertEqual(iface.get('addr'), '127.0.0.1')

    def test_iface_class(self):
        self.assertIsNotNone(FelixTestCase.ip)
        self.assertIsNotNone(FelixTestCase.iface)
        self.assertIsNotNone(FelixTestCase.env.get('IP'))
        self.assertIsNotNone(FelixTestCase.env.get('IFACE'))
        self.assertEqual(FelixTestCase.ip, FelixTestCase.env.get('IP'))
        self.assertEqual(FelixTestCase.iface, FelixTestCase.env.get('IFACE'))


if __name__ == '__main__':
    unittest.main()
