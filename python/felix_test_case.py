#!/usr/bin/env python3

import atexit
import codecs
import glob
import netifaces
import os
import random
import shutil
import socket
import subprocess32 as subprocess
import sys
import unittest
import uuid

from time import sleep

from deepdiff import DeepDiff

from felix_fid import FID


class FelixTestCase(unittest.TestCase):

    @classmethod
    def get_did(cls):
        return 0xF0

    @classmethod
    def get_cid(cls):
        """Return a random cid"""
        cid = random.randint(1, 0xFFFF)
        return cid

    @classmethod
    def get_fid(cls, did, cid, elink, sid=0x00, is_to_flx=False, is_virtual=False):
        """Return an FID"""
        fid = FID.get_fid(did, cid, elink, sid, is_to_flx, is_virtual)
        return fid

    @classmethod
    def get_fid_fm(cls, did, cid, link, sid=0x00, is_to_flx=False, is_virtual=False):
        """Return a fullmode FID"""
        fid = FID.get_fid(did, cid, link, sid, is_to_flx, is_virtual)
        return fid

    @classmethod
    def get_uuid(cls):
        """Return a UUID"""
        return uuid.uuid4().hex

    @classmethod
    def get_port(cls):
        """Return a free port"""
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.bind(('', 0))
        addr = s.getsockname()
        # print addr
        s.close()
        return str(addr[1])

    @classmethod
    def get_inet_iface(cls, iface_names):
        """Return fastest connected interface."""
        # print(netifaces.interfaces())
        ifaces = netifaces.interfaces()
        for iface in iface_names:
            if iface not in ifaces:
                continue
            # print(iface)
            address = netifaces.ifaddresses(iface)
            if not address:
                continue

            afinet = address.get(netifaces.AF_INET)
            if not afinet:
                continue

            first_iface = afinet[0] if len(afinet) else None
            if not first_iface:
                continue

            addr = first_iface.get('addr')
            if not addr:
                continue

            return dict(first_iface, name=iface)
        return None

    @classmethod
    def setUpClass(cls):
        atexit.register(FelixTestCase.tearDownClass)

        random.seed()

        FelixTestCase.regmap_version = os.getenv('REGMAP_VERSION', "0x0400")
        FelixTestCase.netio_protocol = os.getenv('NETIO_PROTOCOL', "libfabric")
        FelixTestCase.netio_type = os.getenv('NETIO_TYPE', "LIBFABRIC")
        FelixTestCase.netio_mode = os.getenv('NETIO_MODE', "RDMA")

        # General
        did = FelixTestCase.get_did()
        FelixTestCase.did = hex(did).rstrip('L')
        cid = FelixTestCase.get_cid()
        FelixTestCase.cid = hex(cid).rstrip('L')
        alt_cid = FelixTestCase.get_cid()
        FelixTestCase.alt_cid = hex(alt_cid).rstrip('L')

        stream_id = 0x9a
        hw_stream_id = 0x00
        hw_alt_stream_id = 0x01
        alt_stream_id = 0xd8
        elink = 8
        hw_elink = 64  # NGU addition 0x40
        alt_elink = 22
        link = 64
        alt_link = 128
        ttc2h_elink = 0x33b if int(FelixTestCase.regmap_version, 0) < 0x0500 else 0x600
        sca_elink = 0x25    # making sure it falls into a valid region for mode 0
        trailer_size = 2 if int(FelixTestCase.regmap_version, 0) < 0x0500 else 4

        FelixTestCase.hw_elink = hex(hw_elink).rstrip('L')
        FelixTestCase.elink = hex(elink).rstrip('L')
        FelixTestCase.alt_elink = hex(alt_elink).rstrip('L')
        FelixTestCase.sca_elink = hex(sca_elink).rstrip('L')
        FelixTestCase.link = hex(link).rstrip('L')
        FelixTestCase.alt_link = hex(alt_link).rstrip('L')
        FelixTestCase.fid = hex(FelixTestCase.get_fid(did, cid, elink)).rstrip('L')
        FelixTestCase.fid_10 = hex(FelixTestCase.get_fid(did, cid, elink+10)).rstrip('L')
        FelixTestCase.fid_sid = hex(FelixTestCase.get_fid(did, cid, elink, stream_id)).rstrip('L')
        FelixTestCase.fid_alt_sid = hex(FelixTestCase.get_fid(did, cid, elink, alt_stream_id)).rstrip('L')
        FelixTestCase.alt_fid = hex(FelixTestCase.get_fid(did, cid, alt_elink)).rstrip('L')
        FelixTestCase.alt_fid_sid = hex(FelixTestCase.get_fid(did, cid, alt_elink, stream_id)).rstrip('L')
        FelixTestCase.alt_fid_alt_sid = hex(FelixTestCase.get_fid(did, cid, alt_elink, alt_stream_id)).rstrip('L')
        FelixTestCase.fid_fm = hex(FelixTestCase.get_fid_fm(did, cid, link)).rstrip('L')
        FelixTestCase.fid_sid_fm = hex(FelixTestCase.get_fid_fm(did, cid, link, stream_id)).rstrip('L')
        FelixTestCase.hw_fid_sid_fm = hex(FelixTestCase.get_fid_fm(did, cid, link, hw_stream_id)).rstrip('L')
        FelixTestCase.fid_alt_sid_fm = hex(FelixTestCase.get_fid_fm(did, cid, link, alt_stream_id)).rstrip('L')
        FelixTestCase.hw_fid_alt_sid_fm = hex(FelixTestCase.get_fid_fm(did, cid, link, hw_alt_stream_id)).rstrip('L')
        FelixTestCase.alt_fid_fm = hex(FelixTestCase.get_fid_fm(did, cid, alt_link)).rstrip('L')
        FelixTestCase.alt_fid_sid_fm = hex(FelixTestCase.get_fid_fm(did, cid, alt_link, stream_id)).rstrip('L')
        FelixTestCase.alt_fid_alt_sid_fm = hex(FelixTestCase.get_fid_fm(did, cid, alt_link, alt_stream_id)).rstrip('L')
        FelixTestCase.fid_toflx = hex(FelixTestCase.get_fid(did, cid, elink, 0x00, True)).rstrip('L')
        FelixTestCase.alt_fid_toflx = hex(FelixTestCase.get_fid(did, cid, alt_elink, 0x00, True)).rstrip('L')
        FelixTestCase.ttc2h_elink = hex(ttc2h_elink).rstrip('L')
        FelixTestCase.fid_sca_toflx = hex(FelixTestCase.get_fid(did, cid, sca_elink, 0x00, True)).rstrip('L')
        FelixTestCase.fid_sca = hex(FelixTestCase.get_fid(did, cid, sca_elink, 0x00, False)).rstrip('L')
        FelixTestCase.trailer_size = str(trailer_size)

        FelixTestCase.vfid = hex(FelixTestCase.get_fid(did, cid, elink, 0x00, False, True)).rstrip('L')
        FelixTestCase.cmd_fid = FelixTestCase.fid
        FelixTestCase.cmd_port = FelixTestCase.get_port()
        FelixTestCase.pub_port = FelixTestCase.get_port()
        FelixTestCase.mon_port = FelixTestCase.get_port()

        FelixTestCase.alt_cmd_fid = hex(FelixTestCase.get_fid(did, alt_cid, elink)).rstrip('L')

        print("Elink: " + FelixTestCase.elink)
        print("FID: " + FelixTestCase.fid)
        print("FID_SID: " + FelixTestCase.fid_sid)
        print("FID_TOFLX: " + FelixTestCase.fid_toflx)
        print("FID_FM: " + FelixTestCase.fid_fm)
        print("FID_SID_FM: " + FelixTestCase.fid_sid_fm)

        FelixTestCase.uuid = FelixTestCase.get_uuid()
        FelixTestCase.tmp_prefix = '/tmp/log-' + FelixTestCase.uuid
        FelixTestCase.port = FelixTestCase.get_port()
        FelixTestCase.toflx_port = FelixTestCase.get_port()
        FelixTestCase.alt_port = FelixTestCase.get_port()
        FelixTestCase.ttc_port = FelixTestCase.get_port()
        FelixTestCase.dcs_port = FelixTestCase.get_port()
        print("UUID: " + FelixTestCase.uuid)
        print("TMP_PREFIX: " + FelixTestCase.tmp_prefix)

        FelixTestCase.env = dict(
            os.environ,
            REGMAP_VERSION=FelixTestCase.regmap_version,
            DID=FelixTestCase.did,
            CID=FelixTestCase.cid,
            ALT_CID=FelixTestCase.alt_cid,
            ELINK=FelixTestCase.elink,
            HW_ELINK=FelixTestCase.hw_elink,
            ALT_ELINK=FelixTestCase.alt_elink,
            LINK=FelixTestCase.link,
            ALT_LINK=FelixTestCase.alt_link,
            TTC2H_ELINK=FelixTestCase.ttc2h_elink,
            SCA_ELINK=FelixTestCase.sca_elink,
            TRAILER_SIZE=FelixTestCase.trailer_size,
            FID=FelixTestCase.fid,
            FID_10=FelixTestCase.fid_10,
            FID_SID=FelixTestCase.fid_sid,
            FID_ALT_SID=FelixTestCase.fid_alt_sid,
            ALT_FID=FelixTestCase.alt_fid,
            ALT_FID_SID=FelixTestCase.alt_fid_sid,
            ALT_FID_ALT_SID=FelixTestCase.alt_fid_alt_sid,
            FID_FM=FelixTestCase.fid_fm,
            FID_SID_FM=FelixTestCase.fid_sid_fm,
            HW_FID_SID_FM=FelixTestCase.hw_fid_sid_fm,
            FID_ALT_SID_FM=FelixTestCase.fid_alt_sid_fm,
            HW_FID_ALT_SID_FM=FelixTestCase.hw_fid_alt_sid_fm,
            ALT_FID_FM=FelixTestCase.alt_fid_fm,
            ALT_FID_SID_FM=FelixTestCase.alt_fid_sid_fm,
            ALT_FID_ALT_SID_FM=FelixTestCase.alt_fid_alt_sid_fm,
            FID_TOFLX=FelixTestCase.fid_toflx,
            ALT_FID_TOFLX=FelixTestCase.alt_fid_toflx,
            FID_SCA_TOFLX=FelixTestCase.fid_sca_toflx,
            FID_SCA=FelixTestCase.fid_sca,
            VFID=FelixTestCase.vfid,
            PORT=FelixTestCase.port,
            TOFLX_PORT=FelixTestCase.toflx_port,
            ALT_PORT=FelixTestCase.alt_port,
            TTC_PORT=FelixTestCase.ttc_port,
            DCS_PORT=FelixTestCase.dcs_port,
            CMD_FID=FelixTestCase.cmd_fid,
            CMD_PORT=FelixTestCase.cmd_port,
            PUB_PORT=FelixTestCase.pub_port,
            MON_PORT=FelixTestCase.mon_port,
            ALT_CMD_FID=FelixTestCase.alt_cmd_fid,
            UUID=FelixTestCase.uuid,
            TMP_PREFIX=FelixTestCase.tmp_prefix,
            NETIO_PROTOCOL=FelixTestCase.netio_protocol,
            NETIO_TYPE=FelixTestCase.netio_type,
            NETIO_MODE=FelixTestCase.netio_mode
        )

        # FelixCard / Hardware
        FelixTestCase.card = False
        FelixTestCase.hardware = os.getenv('FELIX_HARDWARE', False)
        try:
            with open('/proc/flx') as f:
                f.readlines()
                FelixTestCase.card = True
        except IOError:
            pass

        FelixTestCase.env = dict(
            FelixTestCase.env,
            CARD=str(FelixTestCase.card),
            HARDWARE=str(FelixTestCase.hardware)
        )

        # Network
        felix_interface = os.environ.get('FELIX_IFACE')
        iface_list = [felix_interface] if felix_interface else ['vlan109', 'priv0', 'priv1', 'ctrl0', 'eth0']
        FelixTestCase.ip = None
        FelixTestCase.iface = None
        iface = FelixTestCase.get_inet_iface(iface_list)
        print(iface)

        if iface:
            FelixTestCase.ip = iface['addr']
            FelixTestCase.iface = iface['name']
            FelixTestCase.env = dict(
                FelixTestCase.env,
                IP=iface['addr'],
                IFACE=iface['name']
            )

        # Supervisord
        FelixTestCase.supervisord = None
        script_dir = sys.path[0]
        supervisord = os.path.join(script_dir, 'supervisord.conf')
        if os.path.exists(supervisord):
            FelixTestCase.supervisord = supervisord
            print("Start supervisord")
            try:
                subprocess.check_output("supervisord --configuration " + FelixTestCase.supervisord,
                                        env=FelixTestCase.env, stderr=subprocess.STDOUT, shell=True, encoding='UTF-8')
            except subprocess.CalledProcessError as e:
                print(e.returncode)
                print(e.cmd)
                print(e.output)

    @classmethod
    def tearDownClass(cls):
        if FelixTestCase.supervisord:
            print("Stop supervisord")
            try:
                subprocess.check_output("supervisorctl --configuration " + FelixTestCase.supervisord + " shutdown",
                                        env=FelixTestCase.env, stderr=subprocess.STDOUT, shell=True, encoding='UTF-8')
            except subprocess.CalledProcessError as e:
                print(e.returncode)
                print(e.cmd)
                print(e.output)

            # remove pid file
            try:
                pid_file = "/tmp/" + FelixTestCase.uuid + "-supervisord.pid"
                if (os.path.isfile(pid_file)):
                    os.remove(pid_file)
            except FileNotFoundError as e:
                print(e)

            # remove bus
            bus_dir = FelixTestCase.tmp_prefix + '-bus'
            shutil.rmtree(bus_dir, ignore_errors=True)

            # print log and remove
            log_path = '/tmp/' + FelixTestCase.uuid
            cls.print_file(log_path, 'supervisord')
            cls.remove_file(log_path, 'supervisord')

            # remove log files
            log_files = glob.glob("/tmp/log-" + FelixTestCase.uuid + "-*.log")
            for log_file in log_files:
                try:
                    os.remove(log_file)
                except OSError:
                    pass

    @classmethod
    def print_file(cls, path, name, postfix=None, ext='log'):
        pathname = path + '-' + name + ('-' + postfix if postfix else '') + '.' + ext
        print('-- ' + pathname)
        if (os.path.isfile(pathname)):
            print('-- start ' + str(postfix) + ' --')
            with codecs.open(pathname, 'r', encoding='utf-8', errors='ignore') as f:
                print(f.read())
            print('-- end ' + str(postfix) + ' --')

    @classmethod
    def remove_file(cls, path, name, postfix=None, ext='log'):
        pathname = path + '-' + name + ('-' + postfix if postfix else '') + '.' + ext
        if (os.path.isfile(pathname)):
            os.remove(pathname)
            old_logs = glob.glob(pathname + '.*')
            for old_log in old_logs:
                os.remove(old_log)

    def start(self, name, hardware=None):
        if not FelixTestCase.supervisord:
            return

        if FelixTestCase.hardware and hardware:
            name = hardware

        print("Start " + name)
        try:
            output = subprocess.check_output("supervisorctl --configuration " + FelixTestCase.supervisord + " start " + name,
                                             env=FelixTestCase.env, stderr=subprocess.STDOUT, shell=True, encoding='UTF-8')
            self.assertEqual(output, name + ': started\n')
        except subprocess.CalledProcessError as e:
            print(e.returncode)
            print(e.cmd)
            print(e.output)
            self.print_file(FelixTestCase.tmp_prefix, name, 'stdout')
            self.print_file(FelixTestCase.tmp_prefix, name, 'stderr')
            self.remove_file(FelixTestCase.tmp_prefix, name, 'stdout')
            self.remove_file(FelixTestCase.tmp_prefix, name, 'stderr')
            self.assertEqual(e.returncode, 0)

    def stop(self, name, hardware=None):
        if not FelixTestCase.supervisord:
            return

        if FelixTestCase.hardware and hardware:
            name = hardware

        print("Stop " + name)
        try:
            subprocess.check_output("supervisorctl --configuration " + FelixTestCase.supervisord + " stop " + name,
                                    env=FelixTestCase.env, stderr=subprocess.STDOUT, shell=True, encoding='UTF-8')
            # self.assertEqual(output, name + ': stopped\n')
        except subprocess.CalledProcessError:
            # ignored
            pass
        self.print_file(FelixTestCase.tmp_prefix, name, 'stdout')
        self.print_file(FelixTestCase.tmp_prefix, name, 'stderr')
        self.remove_file(FelixTestCase.tmp_prefix, name, 'stdout')
        self.remove_file(FelixTestCase.tmp_prefix, name, 'stderr')

    def assertFile(self, expected_filename, filename, msg=None):
        """Open the file in text mode, compare it."""
        expected_data = None
        with open(expected_filename, mode='r', encoding='UTF-8') as f:
            expected_data = f.read()
        self.assertIsNotNone(expected_data)
        data = None
        with open(filename, mode='r', encoding='UTF-8') as f:
            data = f.read()
        self.assertIsNotNone(data)
        self.assertEqual(expected_data, data)

    def wait_file(self, wfile, timeout):
        expired = 0
        while not os.path.exists(wfile):
            sleep(0.5)
            expired += 1
            if expired == 2 * timeout:
                print("ERROR (FelixTestCase::wait_file) Requested path", wfile, "does not exit.")
                exit(1)
        return

    def assertJSON(self, expected_json, json, msg=None, exclude_paths=None):
        """Check differences of json"""
        diff = DeepDiff(expected_json, json, ignore_order=True, report_repetition=True, exclude_paths=exclude_paths)
        self.assertFalse(diff, diff)
